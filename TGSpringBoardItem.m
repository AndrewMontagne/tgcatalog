//
//  TGSpringBoardItem.m
//  TGCatalog
//
//  Created by Joshua O'Rourke on 15/11/2011.
//  Copyright (c) 2011 Triangulum Studios. All rights reserved.
//

#import "TGSpringBoardItem.h"
#import "UIImage+Extras.h"

@implementation TGSpringBoardItem

- (id)initWithImage:(UIImage*)_image title:(NSString*)_title rect:(CGRect)rect style:(TGSpringBoardStyle *)_style
{
	self = [super initWithFrame:rect];
	[self setOpaque: NO];
	image = _image;
	style = _style;
	selectedImage = [_image tintedImageUsingColor:style.tintColor];
	title = _title;
	
	[self addTarget:self action:@selector(wasTouched) forControlEvents:UIControlEventTouchDown|UIControlEventTouchDragInside];
	[self addTarget:self action:@selector(wasUntouched) forControlEvents:UIControlEventTouchUpInside|UIControlEventTouchDragOutside];
	
	
	
	return self;
}

- (void)wasTouched
{
	isSelected = YES;
	[self setNeedsDisplay];
}

- (void)wasUntouched
{
	isSelected = NO;
	[self setNeedsDisplay];
}

- (void)setSelected:(BOOL)selected
{
	[super setSelected:selected];
	[self setNeedsDisplay];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
	CGSize mySize = [self frame].size;
	CGSize textSize = [title sizeWithFont:style.labelFont constrainedToSize:mySize lineBreakMode:UILineBreakModeTailTruncation];
	int imageOffset = (mySize.width - style.imageSize.width) / 2;
	
	int textYOffset = style.imageSize.height + (((mySize.height - style.imageSize.height) - textSize.height) / 2);
	UIImage* _image;
	
	if(isSelected)
		_image = selectedImage;
	else
		_image = image;
	
	CGContextSetShadowWithColor(UIGraphicsGetCurrentContext(), CGSizeMake(0, 2), 2, 
								[UIColor colorWithRed:0 green:0 blue:0 alpha:.5].CGColor);
	
	[_image drawInRect:CGRectMake(imageOffset, 0, style.imageSize.width, style.imageSize.height)];
	
	[[style labelColor] setFill];
	[title drawInRect:CGRectMake(0, textYOffset, mySize.width, textSize.height) withFont:style.labelFont lineBreakMode:UILineBreakModeTailTruncation alignment:UITextAlignmentCenter];
}


@end
