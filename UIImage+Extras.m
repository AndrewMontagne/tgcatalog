//
//  UIImage+Resize.m
//  TGCatalog
//
//  Created by Joshua O'Rourke on 16/11/2011.
//  Copyright (c) 2011 Triangulum Studios. All rights reserved.
//

#import "UIImage+Extras.h"

@implementation UIImage (Resize)

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize 
{
    UIGraphicsBeginImageContext(newSize);
	CGFloat scale = [[UIScreen mainScreen] scale];
    [image drawInRect:CGRectMake(0, 0, newSize.width * scale, newSize.height * scale)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();    
    UIGraphicsEndImageContext();
	if(scale != 1)
	{
		UIImage *scaleImage = [UIImage imageWithCGImage:image.CGImage scale:scale orientation:UIImageOrientationUp];
		return scaleImage;
	}
	else
		return newImage;
}

- (UIImage *)tintedImageUsingColor:(UIColor *)tintColor 
{
    UIGraphicsBeginImageContext(self.size);
    CGRect drawRect = CGRectMake(0, 0, self.size.width, self.size.height);
    [self drawInRect:drawRect];
    [tintColor set];
    UIRectFillUsingBlendMode(drawRect, kCGBlendModeSourceAtop);
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return tintedImage;
}

@end
