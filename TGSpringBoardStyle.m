//
//  TGSpringBoardStyle.m
//  TGCatalog
//
//  Created by Joshua O'Rourke on 06/02/2012.
//  Copyright (c) 2012 Triangulum Studios. All rights reserved.
//

#import "TGSpringBoardStyle.h"

@implementation TGSpringBoardStyle

@synthesize labelFont, labelColor, tintColor, imageSize;

+ (TGSpringBoardStyle*) defaultStyle
{
	TGSpringBoardStyle * style;
	
	if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) //iPhone / iPod Touch
	{
		style = [[TGSpringBoardStyle alloc] initWithFont:[UIFont boldSystemFontOfSize:11] 
											  labelColor:[UIColor whiteColor] 
											   tintColor:[UIColor colorWithRed:.25 green:.25 blue:.25 alpha:.33] 
											   imageSize:CGSizeMake(57, 57)];
	}
	else //iPad
	{
		style = [[TGSpringBoardStyle alloc] initWithFont:[UIFont boldSystemFontOfSize:16] 
											  labelColor:[UIColor whiteColor] 
											   tintColor:[UIColor colorWithRed:.25 green:.25 blue:.25 alpha:.33] 
											   imageSize:CGSizeMake(72, 72)];
	}
	
	return style;
}

- (TGSpringBoardStyle*) initWithFont:(UIFont*)_font 
						  labelColor:(UIColor*)_labelColor 
						   tintColor:(UIColor*)_tintColor 
						   imageSize:(CGSize)_imageSize
{
	labelFont = _font;
	labelColor = _labelColor;
	tintColor = _tintColor;
	imageSize = _imageSize;
	
	return self;
}

@end
