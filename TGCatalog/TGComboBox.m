//
//  TGComboBox.m
//  TGCatalog
//
//  Created by Joshua O'Rourke on 10/10/2011.
//  Copyright (c) 2011 Triangulum Studios. All rights reserved.
//

#import "TGComboBox.h"
#import "TGComboBoxInputView.h"

@implementation TGComboBox

@synthesize options, value, prompt, delegate = _delegate;

static TGComboBoxInputView * staticInput = nil;

#pragma mark - Initialisation

- (void)setup
{
    [self setBackgroundImage:[[UIImage imageNamed:@"comboBox.png"] stretchableImageWithLeftCapWidth:50 topCapHeight:0] 
                    forState:UIControlStateNormal];
    [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [[self titleLabel] setFont:[UIFont systemFontOfSize:20]];
    [[self titleLabel] setTextAlignment:UITextAlignmentLeft];
    [self addTarget:self action:@selector(onPress) forControlEvents:UIControlEventTouchUpInside];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        // Initialization code
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

#pragma mark - Property overrides

- (void)setValue:(NSString *)newValue
{
    value = newValue;
    [self setTitle:value forState:UIControlStateNormal];
}

- (void)setOptions:(NSArray *)newOptions
{
    options = newOptions;
    [self setValue:[options objectAtIndex:0]];
}

- (BOOL)canBecomeFirstResponder 
{ 
    return YES; 
}

- (UIView*)inputView 
{ 
    if (staticInput == nil) 
    {
        staticInput = [[TGComboBoxInputView alloc] initWithFrame:CGRectMake(0, 0, 320, 260)]; 
    }
    staticInput.comboBox = self;
    return staticInput;
}

#pragma mark - Other

- (void)onPress
{
    [self becomeFirstResponder];
}

@end
