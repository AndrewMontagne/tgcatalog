//
//  TGComboBox.h
//  TGCatalog
//
//  Created by Joshua O'Rourke on 10/10/2011.
//  Copyright (c) 2011 Triangulum Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TGComboBoxDelegate;

@interface TGComboBox : UIButton
{
    NSArray *options;
    NSString *value;
	NSString *prompt;
    id <TGComboBoxDelegate> delegate;
}

@property (nonatomic, retain)  NSArray *options;
@property (nonatomic, retain)  NSString *value;
@property (nonatomic, retain)  NSString *prompt;
@property (nonatomic, assign)  id <TGComboBoxDelegate> delegate;

@end

@protocol TGComboBoxDelegate <NSObject>

@optional

- (void)comboBoxDidConfirmSelection:(TGComboBox *)comboBox;
- (void)comboBoxDidCancelSelection:(TGComboBox *)comboBox;
- (void)comboBoxDidChangeSelection:(TGComboBox *)comboBox;

@end