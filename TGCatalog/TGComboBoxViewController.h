//
//  TGComboBoxViewController.h
//  TGCatalog
//
//  Created by Joshua O'Rourke on 09/10/2011.
//  Copyright (c) 2011 Triangulum Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGComboBox.h"

@interface TGComboBoxViewController : UIViewController// <UIPickerViewDelegate, UIPickerViewDataSource>
{
    IBOutlet TGComboBox *comboBox;
    IBOutlet TGComboBox *comboBox2;
}

@end
