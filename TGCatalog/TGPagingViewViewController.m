//
//  TGPagingViewViewController.m
//  TGPagingView
//
//  Created by Joshua O'Rourke on 02/08/2011.
//  Copyright 2011 Triangulum Studios. All rights reserved.
//

#import "TGPagingViewViewController.h"

@implementation TGPagingViewViewController

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (NSUInteger) numberOfPagesInPagingView:(TGPagingView*)pagingView
{
    return 14;
}

- (UIView*) viewForPageInPagingView:(TGPagingView*)_pagingView atIndex:(NSUInteger)index;
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.text = [NSString stringWithFormat:@"Page %i",index+1];
    label.textAlignment = UITextAlignmentCenter;
    [label setBackgroundColor:[UIColor colorWithHue:(CGFloat)index / (CGFloat)pageControl.numberOfPages saturation:0.5 brightness:1 alpha:1]];
    return label;
}


#pragma mark - View lifecycle


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"TGPagingView";
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
