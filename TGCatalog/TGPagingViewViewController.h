//
//  TGPagingViewViewController.h
//  TGPagingView
//
//  Created by Joshua O'Rourke on 02/08/2011.
//  Copyright 2011 Triangulum Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGPagingView.h"

@interface TGPagingViewViewController : UIViewController <TGPagingViewDelegate>
{
    IBOutlet TGPagingView *pagingView;
    IBOutlet UIPageControl *pageControl;
}

@end
