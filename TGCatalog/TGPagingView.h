//
//  TGPagingView.h
//  TGPagingView
//
//  Created by Joshua O'Rourke on 02/08/2011.
//  Copyright 2011 Triangulum Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TGPagingViewDelegate;

@interface TGPagingView : UIScrollView <UIScrollViewDelegate>
{
    IBOutlet UIPageControl * _pageControl;
    IBOutlet id <TGPagingViewDelegate> _pagingDelegate;
    
    NSUInteger _currentIndex;
    NSUInteger _numberOfPages;
    BOOL animating;
    
    UIView * previousView;
    UIView * currentView;
    UIView * nextView;
}

- (void)scrollToPageAtIndex:(NSUInteger)index;
- (IBAction)scrollToPreviousPage;
- (IBAction)scrollToNextPage;
- (void)reload;

@property (nonatomic, retain) UIPageControl * pageControl;
@property (nonatomic, readonly) NSUInteger currentIndex;
@property (nonatomic, assign) IBOutlet id <TGPagingViewDelegate> pagingDelegate;

@end

@protocol TGPagingViewDelegate <NSObject>

- (NSUInteger) numberOfPagesInPagingView:(TGPagingView*)pagingView;
- (UIView*) viewForPageInPagingView:(TGPagingView*)pagingView atIndex:(NSUInteger)index;

@optional

- (void) pagingView:(TGPagingView*)pagingView didScrollToPage:(NSUInteger)index;

@end