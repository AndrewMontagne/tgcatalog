//
//  TGComboBoxInputView.h
//  TGCatalog
//
//  Created by Joshua O'Rourke on 11/10/2011.
//  Copyright (c) 2011 Triangulum Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGComboBox.h"

@interface TGComboBoxInputView : UIView <UIPickerViewDelegate, UIPickerViewDataSource>
{
    UIPickerView *picker;
    UIBarButtonItem *cancelButton;
    UIBarButtonItem *doneButton;
	UILabel *title;
	UIBarButtonItem *titleButton;
    TGComboBox *comboBox;
	UIToolbar *toolbar;
    NSString *oldValue;
}

- (void)didPressCancelButton;
- (void)didPressDoneButton;

@property (nonatomic, retain) TGComboBox *comboBox;

@end
