//
//  main.m
//  TGCatalog
//
//  Created by Joshua O'Rourke on 07/10/2011.
//  Copyright (c) 2011 Triangulum Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TGAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TGAppDelegate class]));
    }
}
