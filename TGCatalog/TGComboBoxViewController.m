//
//  TGComboBoxViewController.m
//  TGCatalog
//
//  Created by Joshua O'Rourke on 09/10/2011.
//  Copyright (c) 2011 Triangulum Studios. All rights reserved.
//

#import "TGComboBoxViewController.h"

@implementation TGComboBoxViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"TGComboBox";
    
    comboBox.options = [NSArray arrayWithObjects:
                        @"Antimony",
                        @"Arsenic",
                        @"Aluminium",
                        @"Selenium",
                        @"Hydrogen",
                        @"Oxygen",
                        @"Nitrogen",
                        @"Rhenium",
                        @"Nickel",
                        @"Neodymium",
                        @"Neptunium",
                        @"Germanium",
                        @"Iron",
                        @"Americium",
                        @"Uranium",
                        @"Europium",
                        @"Zirconium",
                        @"Lutenium",
                        @"Vanadium",
                        @"Lanthanum",
                        @"Osmium",
                        @"Astatine",
                        @"Radium",
                        @"Gold",
                        @"Protactinium",
                        @"Indium",
                        @"Gallium",
                        @"Iodine",
                        @"Thorium",
                        @"Thulium",
                        @"Thallium",
                        @"Yttrium",
                        @"Ytterbium",
                        @"Actinium",
                        @"Rubidium",
                        @"Boron",
                        @"Gadolinium",
                        @"Niobium",
                        @"Iridium",
                        @"Stronium",
                        @"Silicon",
                        @"Silver",
                        @"Samarium",
                        @"Bismuth",
                        @"Bromine",
                        @"Lithium",
                        @"Beryllium",
                        @"Barium",
                        @"Holmium",
                        @"Helium",
                        @"Hafnium",
                        @"Erbium",
                        @"Phosphorus",
                        @"Francium",
                        @"Fluorine",
                        @"Terbium",
                        @"Manganese",
                        @"Mercury",
                        @"Molybdenum",
                        @"Magnesium",
                        @"Dysprosium",
                        @"Scandium",
                        @"Cerium",
                        @"Caesium",
                        @"Lead",
                        @"Praseodymium",
                        @"Platinum",
                        @"Plutonium",
                        @"Palladium",
                        @"Promethium",
                        @"Potassium",
                        @"Polonium",
                        @"Tantalum",
                        @"Technetium",
                        @"Titanium",
                        @"Tellurium",
                        @"Cadmium",
                        @"Calcium",
                        @"Chromium",
                        @"Curium",
                        @"Sulphur",
                        @"Californium",
                        @"Fermium",
                        @"Berkelium",
                        @"Mendelevium",
                        @"Einsteinium",
                        @"Nobelium",
                        @"Argon",
                        @"Krypton",
                        @"Neon",
                        @"Radon",
                        @"Xenon",
                        @"Zinc",
                        @"Rhodium",
                        @"Chlorine",
                        @"Carbon",
                        @"Cobalt",
                        @"Copper",
                        @"Tungsten",
                        @"Tin",
                        @"Sodium",
                        @"Bohrium",
                        @"Damnstadtium",
                        @"Hassium",
                        @"Meitnerium",
                        @"Dubnium",
                        @"Lawrencium",
                        @"Seaborgium",
                        @"Copernicium",
                        @"Rutherfordium",
                        @"Ununtrium",
                        @"Ununquadium",
						@"Ununpentium",
						@"Ununhexium",
						@"Ununseptium",
						@"Ununoctium",
						@"Ununennium",
						@"Unbinilium",
						@"Unbiunium",
						@"Unbibium",
						@"Unbitrium",
                        nil];
	comboBox.prompt = @"Select an Element";
    
    comboBox2.options = [NSArray arrayWithObjects:
                         @"Proton",
                         @"Antiproton",
                         @"Neutron",
                         @"Electron",
                         @"Positron",
                         @"Electron Neutrino",
                         @"Muon Neutrino",
                         @"Tau Neutrino",
                         @"Muon",
                         @"Lepton",
                         @"Up Quark",
                         @"Down Quark",
                         @"Strange Quark",
                         @"Charmed Quark",
                         @"Photon",
                         nil];
	comboBox2.prompt = @"Select a Particle";
}

- (void)viewDidAppear:(BOOL)animated
{
	[comboBox becomeFirstResponder];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
