//
//  TGAppDelegate.h
//  TGCatalog
//
//  Created by Joshua O'Rourke on 07/10/2011.
//  Copyright (c) 2011 Triangulum Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navController;

@end
