//
//  TGComboBoxInputView.m
//  TGCatalog
//
//  Created by Joshua O'Rourke on 11/10/2011.
//  Copyright (c) 2011 Triangulum Studios. All rights reserved.
//

#import "TGComboBoxInputView.h"

@implementation TGComboBoxInputView

@synthesize comboBox;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
		
		toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
		[toolbar setBarStyle:UIBarStyleBlackTranslucent];
		//[[toolbar topItem] setTitle:@"Select an item:"];
		
		title = [[UILabel alloc] initWithFrame:
						  CGRectMake(0.0f, 0.0, 180.0f, 25.0f)];
		[title setTextAlignment:UITextAlignmentCenter];
		[title setLineBreakMode:UILineBreakModeMiddleTruncation];
		[title setBackgroundColor:[UIColor clearColor]];
		[title setOpaque:NO];
		[title setFont:[UIFont boldSystemFontOfSize:20]];
		[title setTextColor:[UIColor whiteColor]];
		[title setShadowColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
		[title setShadowOffset:CGSizeMake(0, -1)];
		
		cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(didPressCancelButton)];
	
		doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(didPressDoneButton)];
		
		titleButton = [[UIBarButtonItem alloc] initWithCustomView:title];
		
		UIBarButtonItem *spacer1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
		UIBarButtonItem *spacer2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
		
		[toolbar setItems:[NSArray arrayWithObjects:cancelButton, spacer1, titleButton, spacer2, doneButton, nil]];
		
		[self addSubview:toolbar];
		
        picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 320, 260)];
        picker.dataSource = self;
        picker.delegate = self;
        picker.showsSelectionIndicator = YES;
        [self addSubview:picker];
    }
    return self;
}

- (void)setComboBox:(TGComboBox *)newComboBox
{
    comboBox = newComboBox;
    if(comboBox != nil)
    {
        oldValue = [comboBox value];
        [picker reloadAllComponents];
		[title setText:[comboBox prompt]];
        
        NSUInteger index = [comboBox.options indexOfObjectPassingTest:^(id element, NSUInteger idx, BOOL * stop){ 
            *stop = [(NSString*)element isEqualToString:comboBox.value];
            return (*stop);
        }];
        
        if (index != NSNotFound) 
        {
            [picker selectRow:index inComponent:0 animated:NO];
        }
        else
        {
            [picker selectRow:0 inComponent:0 animated:NO];
        }
    }
}

- (void)didPressCancelButton
{
    [comboBox setValue:oldValue];
    [comboBox resignFirstResponder];
    if ([comboBox.delegate respondsToSelector:@selector(comboBoxDidCancelSelection:)])
        [comboBox.delegate comboBoxDidCancelSelection:comboBox];
    self.comboBox = nil;
}

- (void)didPressDoneButton
{
    [comboBox resignFirstResponder];
    if ([comboBox.delegate respondsToSelector:@selector(comboBoxDidConfirmSelection:)])
        [comboBox.delegate comboBoxDidConfirmSelection:comboBox];
    self.comboBox = nil;
}

#pragma mark - Picker view

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [[comboBox options] count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [[comboBox options] objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [comboBox setValue: [[comboBox options] objectAtIndex:row]];
    if ([comboBox.delegate respondsToSelector:@selector(comboBoxDidChangeSelection:)])
        [comboBox.delegate comboBoxDidChangeSelection:comboBox];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
