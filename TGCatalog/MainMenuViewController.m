//
//  MainMenuViewController.m
//  TGCatalog
//
//  Created by Joshua O'Rourke on 07/10/2011.
//  Copyright (c) 2011 Triangulum Studios. All rights reserved.
//

#import "MainMenuViewController.h"

#import "TGPagingViewViewController.h"
#import "TGComboBoxViewController.h"
#import "TGSignaturePadViewController.h"
#import "TGSpringBoardViewController.h"

@implementation MainMenuViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.title = @"TGCatalog";
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"38-house.png"] style:UIBarButtonItemStylePlain target:nil action:nil];
    pageTitles = [NSArray arrayWithObjects:@"TGPagingView", @"TGComboBox", @"TGSpringBoard", @"TGSignaturePad", @"TGSlider",@"TGTicker", nil];
}

- (void)displayAboutPage
{
    UIAlertView *aboutAlert = [[UIAlertView alloc] initWithTitle:@"About" message:@"TGCatalog is a demonstration of various classes designed and implemented by Triangulum Studios." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles: nil];
    [aboutAlert show];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    switch (section) 
    {
        case 0:
            return [pageTitles count];
        case 1:
            return 1;
        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    switch (indexPath.section) 
    {
        case 0:
            cell.textLabel.text = [pageTitles objectAtIndex:indexPath.row];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.imageView.image = [UIImage imageNamed:@"header.png"];
            break;
        case 1:
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.textLabel.text = @"About";
            break;
            
    }
    
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    UIViewController *detailViewController = nil;
    
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) 
            {
                case 0:
                    detailViewController = [[TGPagingViewViewController alloc] initWithNibName:@"TGPagingViewViewController" bundle:nil];
                    break;
                    
                case 1:
                    detailViewController = [[TGComboBoxViewController alloc] initWithNibName:@"TGComboBoxViewController" bundle:nil];
                    break;
                
                case 2:
                    detailViewController = [[TGSpringBoardViewController alloc] initWithNibName:@"TGSpringBoardViewController" bundle:nil];
                    break;
                
                case 3:
                    detailViewController = [[TGSignaturePadViewController alloc] initWithNibName:@"TGSignaturePadViewController" bundle:nil];
                    break;
                    
                default:
                    [tableView deselectRowAtIndexPath:indexPath animated:YES];
                    return;
                    break;
            }
            break;
        case 1:
            [self displayAboutPage];
            break;
        default:
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            return;
            break;
    }
    
    // ...
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
     
}

@end
