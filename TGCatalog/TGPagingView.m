//
//  TGPagingView.m
//  TGPagingView
//
//  Created by Joshua O'Rourke on 02/08/2011.
//  Copyright 2011 Triangulum Studios. All rights reserved.
//

#import "TGPagingView.h"

@implementation TGPagingView

@synthesize pageControl=_pageControl, currentIndex=_currentIndex, pagingDelegate=__pagingDelegate;

#pragma mark - Initialisation and View Management

- (void)setup
{
    self.pagingEnabled = YES;
    self.showsHorizontalScrollIndicator = NO;
    self.showsVerticalScrollIndicator = NO;
    self.delegate = self;
    _currentIndex = 0;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        // Initialization code
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    //NSLog(@"awoken from nib");
    [super awakeFromNib];
    [self setup];
    [self reload];
    UIPageControl * tempPageControl = _pageControl;
    _pageControl = nil;
    [self setPageControl:tempPageControl];
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [self reload];
    [self setContentOffset:CGPointMake(frame.size.width * _currentIndex, 0) animated:NO];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
}

/*
- (void)dealloc 
{
    if (previousView != nil) 
    {
        [previousView removeFromSuperview];
        [previousView release];
    }
    if (currentView != nil) 
    {
        [currentView removeFromSuperview];
        [currentView release];
    }
    if (nextView != nil) 
    {
        [nextView removeFromSuperview];
        [nextView release];
    }
    self.pageControl = nil;
    //[super dealloc];
}
*/
 
#pragma mark - Property Overrides

- (void)setPageControl:(UIPageControl *)pageControl
{
    if (_pageControl != nil) 
    {
        [_pageControl removeTarget:self action:@selector(pageControlDidChange) forControlEvents:UIControlEventValueChanged];
    }
    
    //[_pageControl release];
    _pageControl = pageControl;
    
    if (_pageControl != nil) 
    {
        //[_pageControl retain];
        [_pageControl addTarget:self action:@selector(pageControlDidChange) forControlEvents:UIControlEventValueChanged];
    }
}

- (void)setPagingDelegate:(id<TGPagingViewDelegate>)pagingDelegate
{
    __pagingDelegate = pagingDelegate;
    [self reload];
}

#pragma mark - Private Control Methods

- (void)pageControlDidChange
{
    [self scrollToPageAtIndex:_pageControl.currentPage];
}

- (void)addView:(UIView*)view atIndex:(NSUInteger)index
{
    view.frame = CGRectMake(self.frame.size.width * (index), 0, self.frame.size.width, self.frame.size.height);
    [self addSubview:view];
}

- (void)updatePageControl
{
    if (_pageControl != nil) 
    {
        [_pageControl setCurrentPage:_currentIndex];
    }
}

#pragma mark - Public Control Methods

- (void)scrollToPageAtIndex:(NSUInteger)index
{
    if (!animating) 
    {
        [self setContentOffset:CGPointMake(self.frame.size.width * index, 0) animated:YES];
        animating = YES;
        if (_pageControl != nil) 
        {
            _pageControl.userInteractionEnabled = NO;
        }
    }
}

- (void)scrollToPreviousPage
{
    if (_currentIndex > 0)
    {
        [self scrollToPageAtIndex:_currentIndex - 1];
    }
}

- (void)scrollToNextPage
{
    if (_currentIndex + 1 < _numberOfPages)
    {
        [self scrollToPageAtIndex:_currentIndex + 1];
    }
}

- (void)reload
{
    if (__pagingDelegate != nil) 
    {
        _numberOfPages = [__pagingDelegate numberOfPagesInPagingView:self];
        //NSLog(@"Number of pages: %u",_numberOfPages);
        [self setContentSize:CGSizeMake(self.frame.size.width * _numberOfPages,self.frame.size.height)];
        if (_currentIndex >= _numberOfPages) _currentIndex = _numberOfPages - 1;
        
        if (previousView != nil) 
        {
            [previousView removeFromSuperview];
            //[previousView release];
            previousView = nil;
        }
        if (_currentIndex > 0) 
        {
            previousView = [__pagingDelegate viewForPageInPagingView:self atIndex:_currentIndex-1];// retain];
            [self addView:previousView atIndex:_currentIndex-1];
        }
        
        if (currentView != nil) 
        {
            [currentView removeFromSuperview];
            //[currentView release];
            currentView = nil;
        }
        if (_numberOfPages > 0) 
        {
            currentView = [__pagingDelegate viewForPageInPagingView:self atIndex:_currentIndex];// retain];
            [self addView:currentView atIndex:_currentIndex];
        }
        if (nextView != nil) 
        {
            [nextView removeFromSuperview];
            //[nextView release];
            nextView = nil;
        }
        if (_currentIndex < _numberOfPages-1) 
        {
            nextView = [__pagingDelegate viewForPageInPagingView:self atIndex:_currentIndex+1];// retain];
            [self addView:nextView atIndex:_currentIndex+1];
        }
    }
    else
    {
        [self setContentSize:self.frame.size];
        _numberOfPages = 1;
        _currentIndex = 0;
        //NSLog(@"pagingdelegate = nil!");
    }
    
    if (_pageControl != nil) 
    {
        _pageControl.numberOfPages = _numberOfPages;
        _pageControl.currentPage = _currentIndex;
    }
}

#pragma mark - Scroll View Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSUInteger newIndex = roundf(self.contentOffset.x / self.bounds.size.width);
    
    if (newIndex != _currentIndex) 
    {
        if ([__pagingDelegate respondsToSelector:@selector(pagingView:didScrollToPage:)])
            [__pagingDelegate pagingView:self didScrollToPage:newIndex];
        [_pageControl setCurrentPage:newIndex];
    }
    
    if(_currentIndex > newIndex)
    {
        if (nextView != nil) 
        {
            [nextView removeFromSuperview];
            //[nextView release];
        }
        nextView = currentView;
        currentView = previousView;
        
        _currentIndex = newIndex;
        
        if (_currentIndex > 0) 
        {
            previousView = [[self pagingDelegate] viewForPageInPagingView:self atIndex:_currentIndex - 1];// retain];
            [self addView:previousView atIndex:_currentIndex-1];
        }
        else
        {
            previousView = nil;
        }
    }
    else if(_currentIndex < newIndex)
    {
        if (previousView != nil) 
        {
            [previousView removeFromSuperview];
            //[previousView release];
        }
        previousView = currentView;
        currentView = nextView;
        
        _currentIndex = newIndex;
        
        if (_currentIndex < _numberOfPages-1) 
        {
            nextView = [[self pagingDelegate] viewForPageInPagingView:self atIndex:_currentIndex + 1];// retain];
            [self addView:nextView atIndex:_currentIndex + 1];
        }
        else
        {
            nextView = nil;
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self updatePageControl];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if (_pageControl != nil) 
    {
        _pageControl.userInteractionEnabled = YES;
    }
    animating = NO;
    [self updatePageControl];
}

@end
