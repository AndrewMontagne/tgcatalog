//
//  TGSpringBoardViewController.m
//  TGCatalog
//
//  Created by Joshua O'Rourke on 26/10/2011.
//  Copyright (c) 2011 Triangulum Studios. All rights reserved.
//

#import "TGSpringBoardViewController.h"

@implementation TGSpringBoardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - TGSpringBoardDelegate & TGSpringBoardDataSource

- (void)didSelectItemAtIndex:(NSUInteger)index inSpringBoard:(TGSpringBoard*)springBoard
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Button Pressed" message:[NSString stringWithFormat:@"Button number %i pressed",index] delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles: nil];
    [alert show];
}

- (TGSpringBoardItem*)itemForIndex:(NSUInteger)index inSpringBoard:(TGSpringBoard*)springBoard
{
    return nil;
}

- (NSUInteger)numberOfPagesinSpringBoard:(TGSpringBoard*)springBoard
{
    return 1;
}

- (NSUInteger)numberOfItemsinPage:(NSUInteger)page inSpringBoard:(TGSpringBoard*)springBoard
{
    return 13;
}

#pragma mark - View lifecycle

- (IBAction)didSelectBackground:(UISegmentedControl*)sender
{
    mountain.hidden = TRUE;
    switch(sender.selectedSegmentIndex)
    {
        case 0:
            self.view.backgroundColor = [UIColor blackColor];
            break;
        case 1:
            self.view.backgroundColor = [UIColor whiteColor];
            break;
        case 2:
            self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
            break;
        case 3:
            mountain.hidden = NO;
            break;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"TGSpringBoard";
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
