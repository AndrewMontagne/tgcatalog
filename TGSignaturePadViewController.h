//
//  TGSignaturePadViewController.h
//  TGCatalog
//
//  Created by Joshua O'Rourke on 09/10/2011.
//  Copyright (c) 2011 Triangulum Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGSignaturePadViewController : UIViewController
{
    IBOutlet UIButton * cancelButton;
    IBOutlet UIButton * clearButton;
    IBOutlet UIButton * undoButton;
    IBOutlet UIButton * doneButton;
}

@end
