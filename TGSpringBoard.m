//
//  TGSpringBoard.m
//  TGCatalog
//
//  Created by Joshua O'Rourke on 26/10/2011.
//  Copyright (c) 2011 Triangulum Studios. All rights reserved.
//

#import "TGSpringBoard.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImage+Extras.h"

@implementation TGSpringBoard

@synthesize columns, rows, iconPx, delegate = _delegate, dataSource = _dataSource;

- (IBAction)didPressButton:(UIView*)sender
{
    [_delegate didSelectItemAtIndex:sender.tag + 1 inSpringBoard:self];
}

/*
- (CGRect)frameForItem:(TGSpringBoardItem*)item atIndex:(NSUInteger)index
{
    NSUInteger x = index % rows;
    NSUInteger y = index / rows;
    
    CGRect frame = CGRectMake(padding.width + ((padding.width + iconSize.width) * x),
                              padding.height + ((padding.height + iconSize.height) * y),iconSize.width,iconSize.height);
    return frame;
}
*/

- (void)setup
{	
    columns = 4;
    rows = 4;
    
    textSize = 12;
    
    CGFloat labelHeight = ceilf([@"Label" sizeWithFont:[UIFont boldSystemFontOfSize:textSize]].height + 1);
    CGFloat labelPadding = roundf(labelHeight / 10);
    
    CGSize size = self.frame.size;
    
    iconPx = 57;
    
    CGSize iconSize = CGSizeMake(iconPx + 4,iconPx + labelHeight + labelPadding + labelPadding);
    
    CGSize padding = CGSizeMake(roundf((size.width  - (iconSize.width*columns)) / (columns + 1)),
                                roundf((size.height - (iconSize.height*rows))   / (rows + 1)));
    
	TGSpringBoardStyle *style = [TGSpringBoardStyle defaultStyle];
	UIImage *image = [UIImage imageWithImage:[UIImage imageNamed:@"BlankIcon.png"] scaledToSize:iconSize];
	
    for (int x = 0; x < columns; x++) 
    {
        for (int y = 0; y < rows; y++) 
        {
			CGRect itemRect = CGRectMake(roundf(padding.width + ((padding.width + iconSize.width) * x)),
										 roundf(padding.height + ((padding.height + iconSize.height) * y)),
										 iconSize.width,iconSize.height);
            TGSpringBoardItem *itemView = [[TGSpringBoardItem alloc] initWithImage:image title:@"TGCatalog" rect:itemRect style:style];

            itemView.tag = x + (y * columns);
			
            [itemView addTarget:self action:@selector(didPressButton:) forControlEvents:UIControlEventTouchUpInside];
			 
            [self addSubview:itemView];
        }
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        // Initialization code
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

@end
