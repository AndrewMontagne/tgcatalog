//
//  TGSignaturePadViewController.m
//  TGCatalog
//
//  Created by Joshua O'Rourke on 09/10/2011.
//  Copyright (c) 2011 Triangulum Studios. All rights reserved.
//

#import "TGSignaturePadViewController.h"

@implementation TGSignaturePadViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"TGSignaturePad";
    
    UIImage *grayButton = [[UIImage imageNamed:@"GrayButton.png"] stretchableImageWithLeftCapWidth:5 topCapHeight:0];
    UIImage *blueButton = [[UIImage imageNamed:@"BlueButton.png"] stretchableImageWithLeftCapWidth:5 topCapHeight:0];
    UIImage *whiteButton = [[UIImage imageNamed:@"WhiteButton.png"] stretchableImageWithLeftCapWidth:5 topCapHeight:0];
    
    [cancelButton setBackgroundImage:grayButton forState:UIControlStateDisabled];
    [cancelButton setBackgroundImage:grayButton forState:UIControlStateNormal];
    [cancelButton setBackgroundImage:whiteButton forState:UIControlStateHighlighted];
    
    [clearButton setBackgroundImage:grayButton forState:UIControlStateDisabled];
    [clearButton setBackgroundImage:grayButton forState:UIControlStateNormal];
    [clearButton setBackgroundImage:whiteButton forState:UIControlStateHighlighted];
    
    [undoButton setBackgroundImage:grayButton forState:UIControlStateDisabled];
    [undoButton setBackgroundImage:grayButton forState:UIControlStateNormal];
    [undoButton setBackgroundImage:whiteButton forState:UIControlStateHighlighted];
    
    [doneButton setBackgroundImage:grayButton forState:UIControlStateDisabled];
    [doneButton setBackgroundImage:blueButton forState:UIControlStateNormal];
    [doneButton setBackgroundImage:whiteButton forState:UIControlStateHighlighted];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
