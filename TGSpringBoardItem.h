//
//  TGSpringBoardItem.h
//  TGCatalog
//
//  Created by Joshua O'Rourke on 15/11/2011.
//  Copyright (c) 2011 Triangulum Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGSpringBoardStyle.h"

@interface TGSpringBoardItem : UIControl
{
	UIImage* image;
	UIImage* selectedImage;
	NSString* title;
	TGSpringBoardStyle *style;
	
	BOOL isSelected;
	
	//TODO: Implement badge and editing
	NSString* badge;
	BOOL isEditing;
}

- (void)wasTouched;
- (void)wasUntouched;

//- (void)configureWithIconSize:(CGFloat)size;
- (id)initWithImage:(UIImage*)_image title:(NSString*)_title rect:(CGRect)rect style:(TGSpringBoardStyle*)_style;

@end
