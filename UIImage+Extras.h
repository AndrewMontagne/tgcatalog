//
//  UIImage+Resize.h
//  TGCatalog
//
//  Created by Joshua O'Rourke on 16/11/2011.
//  Copyright (c) 2011 Triangulum Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Resize)

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
- (UIImage *)tintedImageUsingColor:(UIColor *)tintColor;

@end
