//
//  TGSpringBoardStyle.h
//  TGCatalog
//
//  Created by Joshua O'Rourke on 06/02/2012.
//  Copyright (c) 2012 Triangulum Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TGSpringBoardStyle : NSObject
{
	UIFont*		labelFont;
	UIColor*	labelColor;
	UIColor*	tintColor;
	
	CGSize		imageSize;
}

@property (nonatomic, retain) UIFont *	labelFont;
@property (nonatomic, retain) UIColor * labelColor;
@property (nonatomic, retain) UIColor * tintColor;
@property (nonatomic) CGSize			imageSize;

+ (TGSpringBoardStyle*) defaultStyle;
- (TGSpringBoardStyle*) initWithFont:(UIFont*)_font labelColor:(UIColor*)_labelColor tintColor:(UIColor*)_tintColor imageSize:(CGSize)_imageSize;

@end
