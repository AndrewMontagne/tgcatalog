//
//  TGSpringBoardViewController.h
//  TGCatalog
//
//  Created by Joshua O'Rourke on 26/10/2011.
//  Copyright (c) 2011 Triangulum Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGSpringBoard.h"

@interface TGSpringBoardViewController : UIViewController <TGSpringBoardDelegate, TGSpringBoardDataSource>
{
    IBOutlet UIImageView* mountain;
}

- (IBAction)didSelectBackground:(UISegmentedControl*)sender;

@end
