//
//  TGSpringBoard.h
//  TGCatalog
//
//  Created by Joshua O'Rourke on 26/10/2011.
//  Copyright (c) 2011 Triangulum Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGSpringBoardItem.h"
#import "TGSpringBoardStyle.h"

@protocol TGSpringBoardDelegate, TGSpringBoardDataSource;

@interface TGSpringBoard : UIView
{
    NSUInteger  rows;
    NSUInteger  columns;
    CGFloat     iconPx;
    
    float       textSize;
    UIFont    * textFont;
    UIColor   * normalTextColor;
    UIColor   * pressedTextColor;
    
    IBOutlet id <TGSpringBoardDelegate> delegate;
    IBOutlet id <TGSpringBoardDataSource> dataSource;
}

@property (nonatomic)   NSUInteger  rows;
@property (nonatomic)   NSUInteger  columns;
@property (nonatomic)   CGFloat     iconPx;
@property (nonatomic, assign) IBOutlet id <TGSpringBoardDelegate> delegate;
@property (nonatomic, assign) IBOutlet id <TGSpringBoardDataSource> dataSource;

@end

@protocol TGSpringBoardDelegate <NSObject>

- (void)didSelectItemAtIndex:(NSUInteger)index inSpringBoard:(TGSpringBoard*)springBoard;

@end

@protocol TGSpringBoardDataSource <NSObject>

- (TGSpringBoardItem*)itemForIndex:(NSUInteger)index inSpringBoard:(TGSpringBoard*)springBoard;
- (NSUInteger)numberOfPagesinSpringBoard:(TGSpringBoard*)springBoard;
- (NSUInteger)numberOfItemsinPage:(NSUInteger)page inSpringBoard:(TGSpringBoard*)springBoard;

@end


